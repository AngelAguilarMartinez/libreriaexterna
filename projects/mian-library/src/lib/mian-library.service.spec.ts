import { TestBed } from '@angular/core/testing';

import { MianLibraryService } from './mian-library.service';

describe('MianLibraryService', () => {
  let service: MianLibraryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MianLibraryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
