import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MianLibraryComponent } from './mian-library.component';

describe('MianLibraryComponent', () => {
  let component: MianLibraryComponent;
  let fixture: ComponentFixture<MianLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MianLibraryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MianLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
