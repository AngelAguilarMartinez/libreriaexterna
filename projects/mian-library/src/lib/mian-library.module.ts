import { NgModule } from '@angular/core';
import { MianLibraryComponent } from './mian-library.component';



@NgModule({
  declarations: [MianLibraryComponent],
  imports: [
  ],
  exports: [MianLibraryComponent]
})
export class MianLibraryModule { }
